from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import os
import sys
import numpy as np
import tensorflow as tf
import tempfile
import time
from datasets import dataset_factory
from nets import nets_factory
import nets as nets
from preprocessing import preprocessing_factory
import tensorflow.contrib.slim.nets

slim = tf.contrib.slim

# tf.app.flags.DEFINE_string('infile','D:/generated/test-visualization-2/', 'Image file, one image per line.')
# tf.app.flags.DEFINE_string('infile','D:/generated/test-visualization-hair', 'Image file, one image per line.')
input_dir = 'D:/multirace-final/cropped'
tf.app.flags.DEFINE_string('infile',input_dir, 'Image file, one image per line.')
tf.app.flags.DEFINE_boolean('tfrecord', False, 'Input file is formatted as TFRecord.')
tf.app.flags.DEFINE_string('outfile', input_dir +'/result.txt', 'Output file for prediction probabilities.')

tf.app.flags.DEFINE_integer(
    'batch_size', 16, 'The number of samples in each batch.')

tf.app.flags.DEFINE_integer(
    'max_num_batches', None,
    'Max number of batches to evaluate by default use all.')

tf.app.flags.DEFINE_string(
    'master', '', 'The address of the TensorFlow master to use.')

tf.app.flags.DEFINE_string(
    'checkpoint_path', 'D:/checkpoints-age',
    'The directory where the model was written to or an absolute path to a '
    'checkpoint file.')

tf.app.flags.DEFINE_integer(
    'num_preprocessing_threads', 4,
    'The number of threads used to create the batches.')

tf.app.flags.DEFINE_string(
    'dataset_name', 'age', 'The name of the dataset to load.')

tf.app.flags.DEFINE_string(
    'dataset_split_name', 'train', 'The name of the train/test split.')

tf.app.flags.DEFINE_string(
    'dataset_dir', 'D:/age-cropped/', 'The directory where the dataset files are stored.')


tf.app.flags.DEFINE_integer(
    'labels_offset', 0,
    'An offset for the labels in the dataset. This flag is primarily used to '
    'evaluate the VGG and ResNet architectures which do not use a background '
    'class for the ImageNet dataset.')

tf.app.flags.DEFINE_string(
    'model_name', 'mobilenet_v1', 'The name of the architecture to evaluate.')

tf.app.flags.DEFINE_string(
    'preprocessing_name', None, 'The name of the preprocessing to use. If left '
    'as `None`, then the model_name flag is used.')

tf.app.flags.DEFINE_float(
    'moving_average_decay', None,
    'The decay to use for the moving average.'
    'If left as None, then moving averages are not used.')

tf.app.flags.DEFINE_integer(
    'eval_image_size', None, 'Eval image size')

FLAGS = tf.app.flags.FLAGS


def export_files_from_dir(dir, output_filepath, extensions = ['.jpg', '.JPG', '.png', '.bmp', '.jpeg']):

    try:
        files = os.listdir(dir)
    except:
        return

    output_file = open(output_filepath, 'w')
    for file in files:
        for ext in extensions:
            if file.endswith(ext):

                output_file.write(os.path.join(dir, file) + '\n')
                print(os.path.join(dir, file) )
    output_file.close()


def predict_images(input_file, checkpoint_path, model_name, preprocessing_name, is_tfrecord,
                   dataset_name, dataset_dir, dataset_split_name, eval_image_size,
                   output_file,
                   labels_offset = 0):
    graph = tf.Graph()
    with graph.as_default():

        if is_tfrecord:
            fls = tf.python_io.tf_record_iterator(input_file)
        else:
            fls = [s.strip() for s in open(input_file)]

        if is_tfrecord:
            tf.logging.warn('Image name is not available in TFRecord file.')

        if tf.gfile.IsDirectory(checkpoint_path):
            checkpoint_path = tf.train.latest_checkpoint(checkpoint_path)

        print('Checkpoint path is ', checkpoint_path)
        image_string = tf.placeholder(
            tf.string)  # Entry to the computational graph, e.g. image_string = tf.gfile.FastGFile(image_file).read()

        # image = tf.image.decode_image(image_string, channels=3)
        image = tf.image.decode_jpeg(image_string, channels=3, try_recover_truncated=True,
                                     acceptable_fraction=0.3)  ## To process corrupted image files

        # select the dataset
        dataset = dataset_factory.get_dataset(
            dataset_name, dataset_split_name, dataset_dir)

        # select the network
        network_fn = nets_factory.get_network_fn(
            model_name,
            num_classes=(dataset.num_classes - labels_offset),
            is_training=False)

        # # select the prerpocessing
        preprocessing_name = preprocessing_name or model_name
        image_preprocessing_fn = preprocessing_factory.get_preprocessing(
            preprocessing_name,
            is_training=False)

        print('preprocessing name ', preprocessing_name)
        eval_image_size = eval_image_size or network_fn.default_image_size

        processed_image = image_preprocessing_fn(image, eval_image_size, eval_image_size)

        processed_images = tf.reshape(processed_image, (1, eval_image_size, eval_image_size, 3))

        # images = tf.placeholder(tf.float32, [None, 225, 224, 3])
        #  for the inception networks, load the inception arg scope
        arg_scope = None
        if 'inception' in model_name:
            if model_name == 'inception_resnet_v2':
                arg_scope = nets.inception_resnet_v2.inception_resnet_v2_arg_scope()
            if model_name == 'inception_v4':
                arg_scope = nets.inception_v4.inception_v4_arg_scope()
        if model_name == 'mobilenet_v1':
            arg_scope = nets.mobilenet_v1.mobilenet_v1_arg_scope()

        print("*************** arg scope is ", arg_scope)
        if arg_scope is not None:
            with slim.arg_scope(arg_scope):
                conv_net = nets_factory.networks_map[model_name](processed_images, dataset.num_classes)
        else:
            conv_net = nets_factory.networks_map[model_name](processed_images, dataset.num_classes)


        # conv_net = nets.vgg.vgg_19(processed_images, dataset.num_classes)

        variables_to_restore = slim.get_variables_to_restore()
        # print(variables_to_restore)

        # variables_to_restore = slim-eval.get_variables_to_restore(exclude=['vgg_19/fc8'])  #
        init_fn = slim.assign_from_checkpoint_fn(checkpoint_path, variables_to_restore)
        # print(init_fn)
        sess = tf.Session(graph=graph)
        # sess = tf.Session()
        init_fn(sess)


        logits, _ = network_fn(processed_images)
        probabilities = tf.nn.softmax(logits)

        fout = sys.stdout
        if output_file is not None:
            fout = open(output_file, 'w')

        h = ['image']
        h.extend(['class%s' % i for i in range(dataset.num_classes)])
        h.append('predicted_class')
        print('\t'.join(h), file=fout)

        start = time.time()
        for fl in fls:
            image_name = None
            try:
                if is_tfrecord is False:
                    x = tf.gfile.FastGFile(fl,'rb').read()  # You can also use x = open(fl).read()
                    image_name = os.path.basename(fl)
                else:
                    example = tf.train.Example()
                    example.ParseFromString(fl)

                    # Note: The key of example.features.feature depends on how you generate tfrecord.
                    x = example.features.feature['image/encoded'].bytes_list.value[0]  # retrieve image string

                    image_name = 'TFRecord'

                probs = sess.run(probabilities, feed_dict={image_string: x})
                # np_image, network_input, probs = sess.run([image, processed_image, probabilities], feed_dict={image_string:x})

            except Exception as e:
                tf.logging.warn('Cannot process image file %s' % fl)
                continue


            probs = probs[0, 0:]
            a = [image_name]
            a.extend(probs)
            a.append(np.argmax(probs))
            print(','.join([str(e) for e in a]), file=fout)

        end = time.time()
        print('Execution time is ', end - start, '; num images ', len(fls))
        sess.close()
        fout.close()


def evaluate_on_all_classes(main_input_dir, output_dir):

    class_dirs = os.listdir(main_input_dir)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for class_dir in class_dirs:

        class_dir_path = os.path.join(main_input_dir, class_dir)

        file = tempfile.NamedTemporaryFile(mode='w+b', delete=False)
        output_txt = file.name
        export_files_from_dir(class_dir_path, output_txt)
        class_dir_path = output_txt
        input_file = output_txt

        model_name = FLAGS.model_name
        checkpoint_path = FLAGS.checkpoint_path
        preprocessing_name = FLAGS.preprocessing_name

        dataset_name = FLAGS.dataset_name
        dataset_dir = FLAGS.dataset_dir
        dataset_split_name = FLAGS.dataset_split_name
        eval_image_size = FLAGS.eval_image_size
        is_tfrecord = FLAGS.tfrecord

        labels_offset = FLAGS.labels_offset

        output_file = os.path.join(output_dir, class_dir + '.txt')
        print ('outfile is ', output_file)
        print('Classify images: input_file: ', class_dir_path, '\n',
              'checkpoint_path', checkpoint_path, ';', 'model_name: ', model_name,
              '; preprocessing_name: ', preprocessing_name, '; is_tfrecord: ', is_tfrecord, ' \n'
                                                                                            'dataset_name: ', dataset_name,
              '; dataset_dir: ', dataset_dir, '; dataset_split_name: ', dataset_split_name,
              'eval_image_size: ', eval_image_size, '\n'
                                                    'output_file: ', output_dir, '\n'
                                                                                  'labels_offset: ', labels_offset)

        predict_images(input_file, checkpoint_path, model_name, preprocessing_name, is_tfrecord,
                       dataset_name, dataset_dir, dataset_split_name, eval_image_size,
                       output_file,
                       labels_offset)


def evaluate_on_single_class():

    input_file = FLAGS.infile
    if os.path.isdir(input_file):
        file = tempfile.NamedTemporaryFile(mode='w+b', delete=False)
        output_txt = file.name
        export_files_from_dir(input_file, output_txt)
        input_file = output_txt

    model_name = FLAGS.model_name
    checkpoint_path = FLAGS.checkpoint_path
    preprocessing_name = FLAGS.preprocessing_name

    dataset_name = FLAGS.dataset_name
    dataset_dir = FLAGS.dataset_dir
    dataset_split_name = FLAGS.dataset_split_name
    eval_image_size = FLAGS.eval_image_size
    is_tfrecord = FLAGS.tfrecord
    output_file = FLAGS.outfile
    labels_offset = FLAGS.labels_offset

    print('Classify images: input_file: ', input_file, '\n',
                        'checkpoint_path', checkpoint_path,';' ,'model_name: ', model_name,
                        '; preprocessing_name: ', preprocessing_name, '; is_tfrecord: ', is_tfrecord,' \n'
                        'dataset_name: ', dataset_name, '; dataset_dir: ',dataset_dir ,'; dataset_split_name: ',dataset_split_name,
                        'eval_image_size: ', eval_image_size, '\n'
                        'output_file: ', output_file , '\n'
                        'labels_offset: ', labels_offset)

    predict_images(input_file, checkpoint_path, model_name, preprocessing_name, is_tfrecord,
                       dataset_name, dataset_dir, dataset_split_name, eval_image_size,
                       output_file,
                       labels_offset)

if __name__ == '__main__':

    main_input_dir = 'D:/work/image-databases/Park/Color_age_18-29_Neutral_bmp/male'
    evaluate_on_single_class()
